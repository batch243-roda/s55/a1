const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");

const auth = require("../auth");


// Check if email already exists
/*
	Steps:
	1. Use mongoose "find" method to find duplicate emails
	2. Use the "then" method to send a response to the frontend application based on the result of the find method.
*/
module.exports.checkEmailExists = (request, response, next) =>{
	// The result is sent back to the frontend via the then method
	return User.find({email:request.body.email}).then(result =>{
		console.log(request.body.email);
		let message = ``;
			// find method returns an array record of matching documents
		if(result.length >0){
			message = `The ${request.body.email} is already taken, please use other email.`
			return response.status(400).json({error: message});
		}
			// No duplicate email found
			// The email is not yet registered in the database.
		else{
			next();
		}
	})
}


module.exports.registerUser = async(request, response) =>{

	// User.find({email: request.body.email}).then(user => user.length > 0 ? return)

	// try {
		
	// } catch (error) {
		
	// }
	// const user = await User.find({email: request.body.email})

	// if(user.length > 0){
	// 	throw Error(`Email already exist!`) 
	// }

	// creates variable "newUser" and instantiates a new 'User' object using mongoose model
	// Uses the information from request body to provide the necessary information.
	let newUser = new User({
		firstName: request.body.firstName,
		lastName: request.body.lastName,
		email: request.body.email,
		/*salt - salt rounds that bcrypt algortihm will run to encrypt the password*/
		password: bcrypt.hashSync(request.body.password, 10),
		mobileNo: request.body.mobileNo
	})

	// Saves the created object to our database
	return newUser.save().then(user => {
		let token = auth.createAccessToken(newUser);
				console.log(token);
			response.json({accessToken: token});
		// response.json(true)
	}).catch(error =>{
		console.log(error);
		response.json({error: error.message})
	})
}
// User Authentication
/*
	Steps:
		1. Check database if the user email exist.
		2. compare the password provided in the login form with the password stored in the database.
*/
module.exports.loginUser = (request, response) =>{
	// The findOne method, returns the first record in the collection that matches the search criteria.

	return User.findOne({email : request.body.email})
	.then(result =>{

		console.log(result);

		if(result === null){
			return response.send({accessToken: 'empty'});
		}
		else{
					// Creates the variable "isPasswordCorrect" to return the result of comparing the login form password and the database password
					// The compareSync method is used to compare a non encrytpted password from the login from to the encrypted password retrieve. It will return true or false value depending on the result.
			const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

			if(isPasswordCorrect){
				let token = auth.createAccessToken(result);
				console.log(token);
				return response.send({accessToken: token});
			}
			else{
				return response.send({accessToken: 'empty'});
			}
		}
	})
}

module.exports.getProfile = (request, response) =>{

	return User.findById(request.body.id).then(result => {
		result.password = "******";
		console.log(result);
		return response.send(result);
	}).catch(error => {
		console.log(error);
		return response.send(error);
	})
}

module.exports.profileDetails = (request, response) =>{
	// user will be object that contains the id and email of the user that is currently logged in.
	const userData = auth.decode(request.headers.authorization);
	console.log(userData);
	

	return User.findById(userData.id).then(result => {
		result.password = "Confindential";
		return response.send(result)
	}).catch(err => {
		return response.send(err);
	})
	
}

// Update role

module.exports.updateRole = (request, response) =>{
	let token = request.headers.authorization;
	let userData = auth.decode(token);

	let idToBeUpdated = request.params.userId;


	if(userData.isAdmin){
		
		return User.findById(idToBeUpdated).then(result => {
			
			let update = {
				isAdmin: !result.isAdmin
			};

			return User.findByIdAndUpdate(idToBeUpdated, update, {new: true}).then(document => {
				document.password = "Confidential";
				return response.send(document)})
				.catch(err =>response.send(err));

		}).catch(err => response.send(err));

	}else{
		return response.send("You don't have access on this page!")
	}
}

// Enrollment

	module.exports.enroll = async (request, response) => {
		const token = request.headers.authorization;
		let userData = auth.decode(token);

		if(!userData.isAdmin){
			let courseId = request.params.courseId

		let data = {
			courseId: courseId,
			userId: userData.id
		}

		console.log(data)

			let isCourseUpdated = await Course.findById(data.courseId).then(result => {
				console.log(result);
				result.enrollees.push({
					userId: data.userId
				})

				result.slots -= 1;

				return result.save().then(success => {
					return true;
				}).catch(err => {return false});

			})
			.catch(err => {
				console.log(err);
				return response.send(false);
			});


			let isUserUpdated = await User.findById(data.userId).then(result => {
				result.enrollments.push({
					courseId: data.courseId
				})

				return result.save().then(success => {
					return true;
				}).catch(err => {return false;})

			}).catch(err => {return response.send(false)});


			(isUserUpdated && isCourseUpdated) ? response.send(true) : response.send("We encountered an error in your enrollment, please try again!")


	}
	else{
		return response.send(false);
	}

}