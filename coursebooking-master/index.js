//Setup dependencies 
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

/*allow access to routes defined within our application*/
const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");
const app = express();

//Database connection 
 mongoose.connect("mongodb+srv://jroda:admin@zuittbatch243-roda.4axrb9t.mongodb.net/courseBookingAPI?retryWrites=true&w=majority", 
 					{
 						useNewUrlParser: true,
 						useUnifiedTopology:true
 	
    				})
 // If a connection error occurred, output in the console
 // console.error.bind(console) allows us to print errors in the browser console and in the terminal
 // "connection error" is the message that will display if an error is encountered
mongoose.connection.on("error", console.error.bind(console, "connection error"));
// Prompts a message in the terminal once the connection is "open" and we are able to successfully connect to our database
mongoose.connection.once('open',() => console.log("Now connected to MongoDB Atlas"));

// Middlewares
//Allows all resources to access our backend application
app.use(cors());
app.use(express());

// express.json() function is a built-in middleware function in express. It parses incoming request with JSON payloads.
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Defines the "/users" to be included for all user routes defined in the userRoutes rout file.
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);




// Will used the defined port number for the application whenever an environment variable is available OR will used port 4000 if none is defined
//This syntax will allow flexibility when using teh application both locally or as a hosted app
//process.env.PORT is that can be assigned by your hosting service 
 app.listen(process.env.PORT || 4000, () =>{
 	console.log(`API is now online on port ${process.env.PORT||4000}`);
 })
