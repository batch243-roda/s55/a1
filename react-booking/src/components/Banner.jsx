import React from 'react';
import { Button, Col, Container, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const Banner = () => {
  return (
    <Row className="text-center">
      <Col>
        <h1>Zuitt Coding Bootcamp</h1>
        <p>Opportunities for everyone, everywhere</p>

        <Button as={Link} to="/courses" variant="primary">
          Enroll
        </Button>
      </Col>
    </Row>
  );
};

export default Banner;
