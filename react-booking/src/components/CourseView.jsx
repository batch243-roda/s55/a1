import React, { useEffect, useState } from 'react';
import { Button, Card, Col, Container, Row } from 'react-bootstrap';
import { Link, useParams } from 'react-router-dom';
import Swal from 'sweetalert2';
import { useCourseContext } from '../hooks/useCourseContext';
import { useUserContext } from '../hooks/useUserContext';

const CourseView = () => {
  const { user } = useUserContext();
  // const { courses } = useCourseContext();

  const [enrollees, setEnrollees] = useState(0);
  const [slots, setSlots] = useState(0);
  const [isAvailable, setIsAvailable] = useState(true);
  const [course, setCourse] = useState(null);
  const { courseId } = useParams();

  console.log(courseId);

  useEffect(() => {
    const fetchCourse = async () => {
      const response = await fetch(`http://localhost:4000/courses/${courseId}`);

      const json = await response.json();

      if (response.ok) {
        setCourse(json);
        setSlots(json.slots);
        console.log(slots);
      }
    };

    fetchCourse();
  }, [courseId]);

  useEffect(() => {
    console.log(slots);
    if (slots === 0) {
      setIsAvailable(false);
      console.log(isAvailable);
    }
    setIsAvailable(true);
  }, [enrollees]);

  const addEnroll = async () => {
    console.log(localStorage.getItem('token'));
    const token = localStorage.getItem('token');

    const response = await fetch(
      `http://localhost:4000/users/enroll/${courseId}`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
      }
    );

    const json = await response.json();

    console.log(json);
    if (response.ok) {
      Swal.fire({
        title: 'Congrats!',
        text: 'You enrolled succesfully!',
        icon: 'success',
      });
    }

    if (slots !== 0) {
      setEnrollees(enrollees + 1);
      setSlots(slots - 1);
    } else {
      alert('No more slots available');
    }
  };

  return (
    <Container>
      {course && (
        <Row className="mt-3">
          <Col>
            <Card className="h-100 p-3">
              <Card.Body>
                <Card.Title>{course.name}</Card.Title>
                <Card.Text className="h6">Description: </Card.Text>
                <Card.Text>{course.description}</Card.Text>

                <Card.Text className="h6">Price: </Card.Text>
                <Card.Text>PHP {course.price}</Card.Text>

                <Card.Text className="h6">Enrollees: </Card.Text>
                <Card.Text>{enrollees}</Card.Text>

                <Card.Text className="h6">Slots available: </Card.Text>
                <Card.Text>{slots}</Card.Text>

                <Card.Subtitle>Class Schedule</Card.Subtitle>
                <Card.Text>8 am - 5 pm</Card.Text>
                {user ? (
                  <Button
                    variant="primary"
                    onClick={addEnroll}
                    disabled={!isAvailable}
                  >
                    Enroll
                  </Button>
                ) : (
                  <Button
                    as={Link}
                    to="/login"
                    variant="primary"
                    onClick={addEnroll}
                    disabled={!isAvailable}
                  >
                    Enroll
                  </Button>
                )}
                {/* <Button
            variant="primary"
            onClick={addSlots}
            disabled={isAvailable}
          >
            Add Slots
          </Button> */}
              </Card.Body>
            </Card>
          </Col>
        </Row>
      )}
    </Container>
  );
};

export default CourseView;
