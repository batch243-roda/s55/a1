import React, { useEffect, useState } from 'react';
import { Button, Card, Col, Container, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { useUserContext } from '../hooks/useUserContext';

const CourseCard = ({ coursesData }) => {
  const [enrollees, setEnrollees] = useState(0);
  const [slots, setSlots] = useState(coursesData.slots);
  const [isAvailable, setIsAvailable] = useState(true);

  const { user } = useUserContext();

  // useEffect(() => {
  //   if (slots === 0) {
  //     setIsAvailable(false);
  //   }

  //   return () => {};
  // }, [enrollees]);

  // const addEnroll = () => {
  //   if (coursesData.slots !== 0) {
  //     setEnrollees(enrollees + 1);
  //     setSlots(slots - 1);
  //   } else {
  //     alert('No more slots available');
  //   }
  // };

  // const addSlots = () => {
  //   setSlots(slots + 1);
  // };
  return (
    <Container>
      <Row className="mt-3">
        <Col>
          <Card className="h-100 p-3">
            <Card.Body>
              <Card.Title>{coursesData.name}</Card.Title>
              <Card.Text className="h6">Description: </Card.Text>
              <Card.Text>{coursesData.description}</Card.Text>

              <Card.Text className="h6">Price: </Card.Text>
              <Card.Text>PHP {coursesData.price}</Card.Text>

              <Card.Text className="h6">Enrollees: </Card.Text>
              <Card.Text>{enrollees}</Card.Text>

              <Card.Text className="h6">Slots available: </Card.Text>
              <Card.Text>{slots}</Card.Text>
              <Button
                as={Link}
                to={`/courses/${coursesData._id}`}
                variant="primary"
                // onClick={addEnroll}
                // disabled={!isAvailable}
              >
                View
              </Button>

              {/* <Button
                variant="primary"
                onClick={addSlots}
                disabled={isAvailable}
              >
                Add Slots
              </Button> */}
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};

export default CourseCard;
