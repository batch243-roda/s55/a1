import React from 'react';
import { Row, Col, Card } from 'react-bootstrap';

const Highlights = () => {
  return (
    <Row className="my-3">
      {/* First Card */}
      <Col xs={12} md={4}>
        <Card className="h-100 p-3">
          <Card.Body>
            <Card.Title>Learn from Home</Card.Title>
            <Card.Text>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Nibh
              venenatis cras sed felis eget velit aliquet sagittis id.
              Ullamcorper a lacus vestibulum sed arcu non. Egestas sed tempus
              urna et pharetra pharetra massa massa. Gravida dictum fusce ut
              placerat orci nulla pellentesque dignissim enim. Sed arcu non odio
              euismod lacinia at quis. Leo vel orci porta non pulvinar neque
              laoreet. Integer quis auctor elit sed vulputate mi sit amet.
              Consectetur adipiscing elit pellentesque habitant morbi tristique
              senectus et netus. Imperdiet massa tincidunt nunc pulvinar sapien
              et ligula. Arcu risus quis varius quam quisque id diam. Neque
              viverra justo nec ultrices dui sapien eget mi.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>

      {/* Second Card */}
      <Col xs={12} md={4}>
        <Card className="h-100  p-3">
          <Card.Body>
            <Card.Title>Study Now, Pay Later</Card.Title>
            <Card.Text>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Nibh
              venenatis cras sed felis eget velit aliquet sagittis id.
              Ullamcorper a lacus vestibulum sed arcu non. Egestas sed tempus
              urna et pharetra pharetra massa massa. Gravida dictum fusce ut
              placerat orci nulla pellentesque dignissim enim. Sed arcu non odio
              euismod lacinia at quis. Leo vel orci porta non pulvinar neque
              laoreet. Integer quis auctor elit sed vulputate mi sit amet.
              Consectetur adipiscing elit pellentesque habitant morbi tristique
              senectus et netus. Imperdiet massa tincidunt nunc pulvinar sapien
              et ligula. Arcu risus quis varius quam quisque id diam. Neque
              viverra justo nec ultrices dui sapien eget mi.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>

      {/* Third Card */}
      <Col xs={12} md={4}>
        <Card className="h-100 p-3">
          <Card.Body>
            <Card.Title>Be Part of Our Community</Card.Title>
            <Card.Text>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Nibh
              venenatis cras sed felis eget velit aliquet sagittis id.
              Ullamcorper a lacus vestibulum sed arcu non. Egestas sed tempus
              urna et pharetra pharetra massa massa. Gravida dictum fusce ut
              placerat orci nulla pellentesque dignissim enim. Sed arcu non odio
              euismod lacinia at quis. Leo vel orci porta non pulvinar neque
              laoreet. Integer quis auctor elit sed vulputate mi sit amet.
              Consectetur adipiscing elit pellentesque habitant morbi tristique
              senectus et netus. Imperdiet massa tincidunt nunc pulvinar sapien
              et ligula. Arcu risus quis varius quam quisque id diam. Neque
              viverra justo nec ultrices dui sapien eget mi.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
};

export default Highlights;
