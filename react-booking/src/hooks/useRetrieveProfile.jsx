import React from 'react';
import Swal from 'sweetalert2';
import { useUserContext } from './useUserContext';

export const useRetrieveProfile = () => {
  const { setUser } = useUserContext();

  const retrieveProfile = async (token) => {
    const response = await fetch(`http://localhost:4000/users/profile`, {
      headers: { Authorization: `Bearer ${token}` },
    });

    const json = await response.json();

    Swal.fire({
      title: `Welcome! ${json.email}`,
      text: 'You can now enroll in our courses!',
      icon: 'success',
    });
    setUser(json);
  };

  return { retrieveProfile };
};
