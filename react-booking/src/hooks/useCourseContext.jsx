import React, { useContext } from 'react';
import { CourseContext } from '../context/CourseContext';

export const useCourseContext = () => {
  const context = useContext(CourseContext);

  if (!context) {
    throw Error('useCourseContext must be used inside a CourseContextProvider');
  }

  return context;
};
