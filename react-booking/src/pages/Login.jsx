import React, { useEffect, useState } from 'react';
import { Button, Col, Container, Form, Row } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import { useRetrieveProfile } from '../hooks/useRetrieveProfile';
import { useUserContext } from '../hooks/useUserContext';

const Login = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isEmpty, setIsEmpty] = useState(true);

  const { retrieveProfile } = useRetrieveProfile();

  const { user, setUser } = useUserContext();

  useEffect(() => {
    if (email && password) {
      setIsEmpty(false);
    } else {
      setIsEmpty(true);
    }
  }, [email, password]);

  const handleSubmit = async (e) => {
    e.preventDefault();

    const response = await fetch(`http://localhost:4000/users/login`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ email, password }),
    });

    const json = await response.json();

    if (json.accessToken !== 'empty') {
      localStorage.setItem('token', json.accessToken);
      retrieveProfile(json.accessToken);
    }

    // alert(`Login Successfully`);
    // localStorage.setItem('email', email);
    // const data = localStorage.getItem('email');
    // setUser(data);
    // setEmail('');
    // setPassword('');
  };

  return user ? (
    <Navigate to="/" />
  ) : (
    <Container>
      <Row className="justify-content-center p-5">
        <Col sm={12} md={6} className="bg-dark p-3 rounded text-light">
          <Form onSubmit={handleSubmit}>
            <Form.Group className="mb-3" controlId="email">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                onChange={(e) => setEmail(e.target.value)}
                value={email}
                required
              />
              <Form.Text className="text-muted">
                We'll never share your email with anyone else.
              </Form.Text>
            </Form.Group>

            <Form.Group className="mb-3" controlId="password">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                onChange={(e) => setPassword(e.target.value)}
                value={password}
                required
              />
            </Form.Group>
            <Button variant="success" type="submit" disabled={isEmpty}>
              Submit
            </Button>
          </Form>
        </Col>
      </Row>
    </Container>
  );
};

export default Login;
