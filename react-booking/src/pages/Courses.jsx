import React, { useEffect, useState } from 'react';
import CourseCard from '../components/CourseCard';
import coursesData from '../data/course';
import { useCourseContext } from '../hooks/useCourseContext';

const Courses = () => {
  const { courses } = useCourseContext();
  return courses.map((course) => (
    <CourseCard key={course._id} coursesData={course} />
  ));
};

export default Courses;
