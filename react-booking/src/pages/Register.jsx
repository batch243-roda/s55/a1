import React, { useEffect, useState } from 'react';
import { Button, Col, Container, Form, Row } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { useRetrieveProfile } from '../hooks/useRetrieveProfile';
// import { retrieveProfile } from '../hooks/useRetrieveProfile';
import { useUserContext } from '../hooks/useUserContext';

const Register = () => {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [number, setNumber] = useState('');
  const [email, setEmail] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');
  const [isEmpty, setIsEmpty] = useState(true);

  const { retrieveProfile } = useRetrieveProfile();

  const { setUser } = useUserContext();

  useEffect(() => {
    if (
      firstName &&
      lastName &&
      number &&
      email &&
      password1 &&
      password2 &&
      password1 === password2
    ) {
      setIsEmpty(false);
    } else {
      setIsEmpty(true);
    }
  }, [firstName, lastName, number, email, password1, password2]);

  const handleSubmit = async (e) => {
    e.preventDefault();

    const response = await fetch('http://localhost:4000/users/register', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        firstName,
        lastName,
        email,
        password: password1,
        mobileNo: number,
      }),
    });

    const json = await response.json();
    console.log(json);
    if (json.error) {
      return Swal.fire({
        title: `${json.error}`,
        icon: 'error',
      });
    }

    if (response.ok) {
      // setUser(json);
      localStorage.setItem('token', json.accessToken);
      retrieveProfile(json.accessToken);
    }
  };

  return (
    <Container>
      <Row className="justify-content-center p-5">
        <Col sm={12} md={6} className="bg-dark p-3 rounded text-light">
          <Form onSubmit={handleSubmit}>
            <Form.Group className="mb-3" controlId="firstName">
              <Form.Label>First Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter first name"
                onChange={(e) => setFirstName(e.target.value)}
                value={firstName}
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="lastName">
              <Form.Label>Last Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter last name"
                onChange={(e) => setLastName(e.target.value)}
                value={lastName}
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="number">
              <Form.Label>Mobile Number</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter number"
                onChange={(e) => setNumber(e.target.value)}
                value={number}
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="email">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                onChange={(e) => setEmail(e.target.value)}
                value={email}
                required
              />
              <Form.Text className="text-muted">
                We'll never share your email with anyone else.
              </Form.Text>
            </Form.Group>

            <Form.Group className="mb-3" controlId="password">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                onChange={(e) => setPassword1(e.target.value)}
                value={password1}
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="confirmPassword">
              <Form.Label>Confirm Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                onChange={(e) => setPassword2(e.target.value)}
                value={password2}
                required
              />
            </Form.Group>
            <Button variant="primary" type="submit" disabled={isEmpty}>
              Submit
            </Button>
          </Form>
        </Col>
      </Row>
    </Container>
  );
};

export default Register;
