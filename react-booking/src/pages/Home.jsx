import React, { Fragment } from 'react';
import { Container } from 'react-bootstrap';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

const Home = () => {
  return (
    <Container>
      <Banner />
      <Highlights />
    </Container>
  );
};

export default Home;
