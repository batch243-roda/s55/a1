import { createContext, useEffect, useState } from 'react';

export const CourseContext = createContext();

export const CourseContextProvider = ({ children }) => {
  const [courses, setCourses] = useState([]);

  useEffect(() => {
    const fetchCourses = async () => {
      const response = await fetch(
        'http://localhost:4000/courses/allActiveCourses'
      );

      const json = await response.json();

      if (response.ok) {
        console.log(json);
        setCourses(json);
      }
    };
    fetchCourses();
  }, []);

  return (
    <CourseContext.Provider value={{ courses, setCourses }}>
      {children}
    </CourseContext.Provider>
  );
};
